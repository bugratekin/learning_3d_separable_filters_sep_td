function [m_mat] = magnify_matrix(in_mat, factor)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

m_mat = zeros(size(in_mat, 1)*factor, size(in_mat, 2)*factor,size(in_mat, 3)*factor);

for r = 1 : size(in_mat, 1)
    for c = 1 : size(in_mat, 2)
        for s = 1: size(in_mat, 3)
            m_mat((r-1)*factor + 1 : r*factor, (c-1)*factor + 1 : c*factor, (s-1)*factor + 1 : s*factor) = in_mat(r, c, s);
        end
    end 
end

end
