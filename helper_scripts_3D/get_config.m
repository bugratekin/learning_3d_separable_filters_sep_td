function [p] = get_config()
%  get_config  Setup file for the approximation algorithm.
%
%  Synopsis:
%     get_config()
%
%  author: Bugra Tekin, CVLab EPFL
%  e-mail: bugra <dot> tekin <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/
%  date: October 2013
%  last revision: 23 December 2013

% Size of the filters in the filter bank
p.filters_size = [13,13,13];

% Number of desired filters in the separable filter bank
p.rank = 16; % rank used in CD decomposition  

% Create folder to store the results
p.resulting_filters_directory = 'results';
[status, message, messageid] = rmdir(p.resulting_filters_directory, 's'); %#ok<*NASGU,*ASGLU>
[status, message, messageid] = mkdir(p.resulting_filters_directory); %#ok<*ASGLU,*NASGU>

% Filter bank path
p.original_filters_txt = 'data_3D/filters_opf_49_13_32_001740.txt'; %txt file containing the original filter bank
p.original_filters_img = 'data_3D/fb_opf_49_001740_join.nrrd'; %txt file containing the original filter bank
p.reconstructed_path = fullfile(p.resulting_filters_directory, sprintf('reconstructed3D_rank%03d.txt', p.rank));
p.separable_path = fullfile(p.resulting_filters_directory, sprintf('separable3D_rank%03d.txt', p.rank)); 

% Parameters for visualization
p.vSpace = 4;
p.hSpace = 4;
p.pixS = 2;

% Set random number stream
s = RandStream('mt19937ar','Seed',9);
RandStream.setGlobalStream(s);

% Set the parameters for the optimization algorithm
p.options = ncg('defaults');
p.options.Update = 'HS'; % Hestenes-Stiefel update

end
