function [img] = convert_img_visualization(in_mat)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

img = zeros(size(in_mat));

min_val = min(in_mat(:));
max_val = max(in_mat(:));
extr = max(abs(min_val),abs(max_val));

if(max_val-min_val<1e-5 || extr<1e-5)
    img(:,:) = 127.5;
else
    img = in_mat./extr;
    img = img+1;
    img = img*127.5;
end

end

