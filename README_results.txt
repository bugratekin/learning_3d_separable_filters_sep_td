------- Results format ----------

The results of the optimization will be saved in the "results" folder.

N.B. Starting a new optimization will delete all previous results in the current folder !

+Format: 

- The filter bank in reconstructed3D_rank_txt files is saved using the same convention used for the input filter bank (see data_3D/README_data.txt).

- The separable components in separable3D_rank_txt is saved as follow: if K is the number of separable filters and R,C,S are respectively the number of rows, columns and slices of the filters, then the .txt file has K columns and R+C+S rows, where if we define A the matrix formed by the first R rows, B is the matrix formed by rows from R+1 to R+S and C is the matrix formed by the last S rows, then the k-th separable filter s_k is given by: s_k = A(:,k)oB(:,k)oC(:,k), where 'o' denotes the vector outer product.

- The coefficients in coeffs_rec_txt/ are saved in a KxN matrix W, where K is the number of separable filters and N the number of filters in the original filter bank. W is such that f_n = sum_k(W(k,n)*s_k), where f_n is the n-th of the original filters and s_k the kith of the separable filters. Using matrices A,B,C defined above we can write f_n = ktensor(W(:,n),{A,B,C}), using the ktensor function of the MATLAB Tensor Toolbox [1].


[1] http://www.sandia.gov/~tgkolda/TensorToolbox/



 