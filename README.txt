3D Low Rank Filters Approximation using Tensor Decomposition

Approximate an existing filter bank with a set of separable filters using tensor decomposition [1].

The algorithm's settings are in the helper_scripts_3D/get_config.m file.

+ Required libraries: 
- Teem/nrrd library ([2]). Once compiled for your
  architecture, copy them in the helper_scripts_3D/nrrd/ subdirectory.
- MATLAB Tensor Toolbox [3] and Poblano toolbox [4]. To use CP_OPT algorithm [5].

+ Run:
Run separable_filter_approximation_3D_td function. The resulting separable
filter bank will be stored in results directory along with the
reconstruction and reconstruction coefficients. The obtained separable
filter banks can then be used for voxel classification to speed up the
process [1].

For any question or bug report, please feel free to contact me at:
bugra <dot> tekin <at> epfl <dot> ch

[1] Learning Separable Filters
[2] http://teem.sourceforge.net/nrrd/index.html
[3] http://www.sandia.gov/~tgkolda/TensorToolbox/
[4] https://software.sandia.gov/trac/poblano/
[5] E. Acar and D. M. Dunlavy and T. G. Kolda "A Scalable Optimization Approach for Fitting Canonical Tensor Decompositions". Journal of Chemometrics, 2011.
