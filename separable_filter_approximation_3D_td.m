function [] = separable_filter_approximation_3D_td()
%  Approximates a 3D filter bank with a separable filter bank.
%
%  Synopsis:
%     separable_filter_approximation_3d_td()
%
%  author: Bugra Tekin, CVLab EPFL
%  e-mail: bugra <dot> tekin <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/
%  date: October 2013
%  last revision: 23 December 2013

%% Initialization

tic;

p = get_config; % <--- MODIFY HERE the algorithm's parameters

% Load original filterbank
[p,original_filters] = load_original_fb(p);

%% CP decomposition

% Apply CP-OPT
[P] = cp_opt( tensor( original_filters ), p.rank, 'alg_options', p.options);

% Find the components of the Kruskal form
A = P.U{1};
B = P.U{2};
C = P.U{3};
D = P.U{4};


%% Compute separable filters

sep = zeros(p.filters_size(1),p.filters_size(2), p.filters_size(3), p.rank);
normSep = zeros(p.rank);
M = zeros(prod(p.filters_size),p.rank);
for i = 1 : p.rank
    filt_2D = P.lambda(i)*A(:,i)*B(:,i)';
    for j=1:p.filters_size(3)
        sep(:,:,j,i) = filt_2D*C(j,i);
    end
    temp = sep(:,:,:,i);
    normSep(i) = norm(temp(:));
    sep(:,:,:,i) = sep(:,:,:,i)/normSep(i);
    filter = sep(:,:,:,i);
    M(:, i) = filter(:);
end

%% Recompose the tensor

% Initialize the reconstructed filter banks
coefs = zeros(p.rank,p.filters_no);
recomp = zeros(p.filters_size(1), p.filters_size(2), p.filters_size(3), p.filters_no);
for i=1:p.filters_no
    sum = 0;
    for j=1:p.rank
        sum = sum + P.lambda(j)*normSep(j)*sep(:,:,:,j)*D(i,j);
        coefs(j,i) = P.lambda(j)*D(i,j)*normSep(j);
    end
    recomp(:,:,:,i) = sum;
end
recomp = double(recomp);
coefs = double(coefs);

cpuExecutionTime = toc;

%% Calculate the error between the original filters and the filters that we obtain after decomposing & composing tensor

sum_err_recomp = 0;
err_recomp = zeros(p.filters_no,1);
for i=1:p.filters_no
    err_recomp(i) = norm(double(original_filters(:,i))-double(recomp(:,i)))^2/(p.filters_size(1)*p.filters_size(2)*p.filters_size(3));
    sum_err_recomp = sum_err_recomp + err_recomp(i);
end

% Find the average error
avg_err_recomp = sum_err_recomp / p.filters_no;

%% Save the reconstructed filters and reconstruction coefficients

rows_no = p.filters_size(1);
cols_no = p.filters_size(2);
sli_no = p.filters_size(3);

% Initialize the filter bank
rec_fb = zeros(rows_no*cols_no*sli_no,p.filters_no);

% Save the reconstructed filters in the text format
for i=1:p.filters_no
    filter = recomp(:,:,:,i);
    rec = filter(:);
    rec_fb(:,i) = rec;
    rec = reshape(rec,p.filters_size);
    for i_sli = 1:p.filters_size(3),
       dlmwrite(p.reconstructed_path, rec(:,:,i_sli), 'delimiter', '\t', 'precision', 16, '-append'); 
    end    
end
fclose('all');

% Save reconstructed fb image
nrrdSave(sprintf('%s/reconstructed3D_rank%03d.nrrd', p.resulting_filters_directory, p.rank), permute(reshape_fb_as_img3D(p, rec_fb, size(rec_fb,2)),[2,1,3]));

% Save the reconstruction coefficients in .txt format
save(sprintf('%s/coefs3D_rank%03d.txt',p.resulting_filters_directory, p.rank),'coefs','-ascii')

%% Save the separable filters in the text format

% Save the separable filters in nrrd format
nrrdSave(sprintf('%s/separable3D_%03d.nrrd', p.resulting_filters_directory, p.rank), permute(reshape_fb_as_img3D(p, M, size(M,2)),[2,1,3])); 

% Efficient way to store separable filters
dlmwrite(p.separable_path, A, 'delimiter', '\t', 'precision', 16); % overwrite existing file
dlmwrite(p.separable_path, B, 'delimiter', '\t', 'precision', 16,'-append');
dlmwrite(p.separable_path, C, 'delimiter', '\t', 'precision', 16,'-append');

%% Output error and execution time

fd = fopen(sprintf('%s/stats3D_rank%03d.txt', p.resulting_filters_directory, p.rank), 'wt');
fprintf(fd, 'The average reconstruction error for rank of %d is %f\n',p.rank, avg_err_recomp);
fprintf(fd ,'Execution time: %f sec\n', cpuExecutionTime);
fclose(fd);
fprintf('The average reconstruction error for rank of %d is %f\n',p.rank, avg_err_recomp);
fprintf('Execution time: %f sec\n', cpuExecutionTime);

end
